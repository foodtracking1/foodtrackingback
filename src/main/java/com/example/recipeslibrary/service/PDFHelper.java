package com.example.recipeslibrary.service;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRSaver;

import java.io.InputStream;

public class PDFHelper {

    public static void compile() throws JRException {
        InputStream employeeReportStream = PDFHelper.class.getResourceAsStream("/static.recipeReport.jrxml");
        JasperReport jasperReport = JasperCompileManager.compileReport(employeeReportStream);
        JRSaver.saveObject(jasperReport,"/static/recipeReport.jasper");
    }
}
