package com.example.recipeslibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecipesLibraryApplication {

    public static void main(String[] args) {

        SpringApplication.run(RecipesLibraryApplication.class, args);
    }

}
