package com.example.recipeslibrary.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Logging {

    private final Logger logger;

    public Logging(Class<?> cl) {
        this.logger = LoggerFactory.getLogger(cl);
    }

    public Logging() {
        this.logger = LoggerFactory.getLogger("Unknown");
    }

    public void trace(String message) {
        this.logger.trace(message);
    }

    public void debug(String message) {
        this.logger.debug(message);
    }

    public void info(String message) {
        this.logger.info(message);
    }

    public void warn(String message) {
        this.logger.warn(message);
    }

    public void error(String message) {
        this.logger.error(message);
    }
}
