package com.example.recipeslibrary.controller;

import com.example.recipeslibrary.exception.ResourceNotFoundException;
import com.example.recipeslibrary.model.Ingredient;
import com.example.recipeslibrary.model.Recipe;
import com.example.recipeslibrary.repository.IngredientRepository;
import com.example.recipeslibrary.repository.RecipeRepository;
import com.example.recipeslibrary.util.Logging;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
public class RecipeController {
    private final RecipeRepository recipeRepository;

    private final IngredientRepository ingredientRepository;

    private final Logging logger = new Logging(RecipeController.class);

    public RecipeController(RecipeRepository recipeRepository, IngredientRepository ingredientRepository) {
        this.recipeRepository = recipeRepository;
        this.ingredientRepository = ingredientRepository;
    }

    /**
     * GET /recipes/{recipeId}/ingredients
     * REST endpoint to retrieve all the ingredients of a recipe
     *
     * @param recipeId the recipe
     * @return the {@link List} of {@link Ingredient} of the recipe
     */
    @GetMapping("/recipes/{recipeId}/ingredients")
    public Page<Ingredient> getIngredientsByRecipeId(@PathVariable UUID recipeId, Pageable pageable) {
        this.logger.info("REST request to get all the ingredients of the recipe with id : " + recipeId.toString());
        Recipe recipeFound = recipeRepository.findById(recipeId)
                .orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
        if ((null != recipeFound) && (recipeFound.getIngredients().size() > 0)) {
            this.logger.info("Recipe found for id : " + recipeId.toString() + ". Retrieving the ingredients of that recipe.");
            List<Ingredient> ingredients = ingredientRepository.findAllById(recipeFound.getIngredients().stream().map(Ingredient::getId).collect(Collectors.toList()));
            return new PageImpl<>(ingredients);
        } else {
            this.logger.warn("No recipe found for id : " + recipeId.toString());
            return new PageImpl<>(new ArrayList<>());
        }
    }

    /**
     * POST /recipes/{recipeId}/ingredients
     * REST endpoint to add an ingredient to a recipe
     *
     * @param recipeId   the recipe
     * @param ingredient the JSON ingredient (body of the request)
     * @return the {@link Recipe} that has been updated.
     */
    @PostMapping("/recipes/{recipeId}/ingredients")
    public Recipe addIngredientByRecipeId(@PathVariable UUID recipeId, @Valid @RequestBody Ingredient ingredient) {
        this.logger.info("REST request to add ingredient to the recipe with id : " + recipeId.toString());
        return recipeRepository.findById(recipeId)
                .map(recipe -> {
                    recipe.addIngredient(ingredient);
                    return recipeRepository.save(recipe);
                }).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
    }

    /**
     * DELETE /recipes/{recipeId}/ingredients/{ingredientId}
     * REST endpoint to delete ingredient from a recipe
     *
     * @param recipeId     the recipe
     * @param ingredientId the ingredient to delete
     * @return a {@link ResponseEntity} OK if the ingredient was deleted.
     */
    @DeleteMapping("/recipes/{recipeId}/ingredients/{ingredientId}")
    public ResponseEntity<?> deleteIngredientByRecipeId(@PathVariable("recipeId") UUID recipeId, @PathVariable("ingredientId") UUID ingredientId) {
        this.logger.info("REST request to delete ingredient from the recipe with id : " + recipeId.toString());
        return ingredientRepository.findById(ingredientId)
                .map(ingredient -> {
                    recipeRepository.findById(recipeId)
                            .map(recipe -> {
                                recipe.removeIngredient(ingredient);
                                return recipeRepository.save(recipe);
                            }).orElseThrow(() -> new ResourceNotFoundException("The ingredient with id : " + ingredientId + " was not found in the recipe with  id " + recipeId));
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("No ingredient found with id " + ingredientId));
    }

    @GetMapping("/recipes")
    public Page<Recipe> getRecipes(Pageable pageable) {
        return recipeRepository.findAll(pageable);
    }

    @PostMapping("/recipes")
    public Recipe createRecipe(@Valid @RequestBody Recipe recipe) {
        return recipeRepository.save(recipe);
    }

    @PutMapping("/recipes/{recipeId}")
    public Recipe updateRecipe(@PathVariable UUID recipeId, @Valid @RequestBody Recipe recipeRequest) throws ResourceNotFoundException {
        return recipeRepository.findById(recipeId)
                .map(recipe -> {
                    recipe.setTitle(recipeRequest.getTitle());
                    recipe.setDescription(recipeRequest.getDescription());
                    recipe.setIngredients(recipeRequest.getIngredients());
                    return recipeRepository.save(recipe);
                })
                .orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId));
    }

    @DeleteMapping("/recipes/{recipeId}")
    public ResponseEntity<?> deleteRecipe(@PathVariable UUID recipeId) throws ResourceNotFoundException {
        return recipeRepository.findById(recipeId)
                .map(recipe -> {
                    recipeRepository.delete(recipe);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Recipe not found with id " + recipeId.toString()));
    }
}
