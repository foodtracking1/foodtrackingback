package com.example.recipeslibrary.controller;

import com.example.recipeslibrary.exception.ResourceNotFoundException;
import com.example.recipeslibrary.model.Ingredient;
import com.example.recipeslibrary.repository.IngredientRepository;
import com.example.recipeslibrary.util.Logging;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@CrossOrigin
@RestController
public class IngredientController {

    private final IngredientRepository ingredientRepository;

    private final Logging logger = new Logging(IngredientController.class);

    public IngredientController(IngredientRepository ingredientRepository) {
        this.ingredientRepository = ingredientRepository;
    }

    @GetMapping("/ingredients")
    public Page<Ingredient> getIngredients(Pageable pageable) {
        this.logger.info("Retrieving all ingredients...");
        return ingredientRepository.findAll(pageable);
    }

    @PostMapping("/ingredients")
    public Ingredient createIngredient(@Valid @RequestBody Ingredient ingredient) {
        this.logger.info("Ingredient " + ingredient.toString() + " will be added to database...");
        return ingredientRepository.save(ingredient);
    }

    @PutMapping("/ingredients/{ingredientId}")
    public Ingredient updateIngredient(@PathVariable UUID ingredientId, @Valid @RequestBody Ingredient ingredientRequest) {
        this.logger.info("Ingredient " + ingredientId.toString() + " will be updated.\n" + ingredientRequest.toString());
        return ingredientRepository.findById(ingredientId)
                .map(ingredient -> {
                    ingredient.setName(ingredientRequest.getName());
                    return ingredientRepository.save(ingredient);
                }).orElseThrow(() -> new ResourceNotFoundException("Ingredient not found with id " + ingredientId));
    }

    @DeleteMapping("/ingredients/{ingredientId}")
    public ResponseEntity<?> deleteIngredient(@PathVariable UUID ingredientId) {
        this.logger.info("Ingredient " + ingredientId.toString() + " will be deleted...");
        return ingredientRepository.findById(ingredientId)
                .map(ingredient -> {
                    ingredientRepository.delete(ingredient);
                    return ResponseEntity.ok().build();
                }).orElseThrow(() -> new ResourceNotFoundException("Ingredient not found with id " + ingredientId));
    }
}
