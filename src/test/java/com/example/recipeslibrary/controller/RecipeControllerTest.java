package com.example.recipeslibrary.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RecipeControllerTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getIngredientsByRecipeId() {
    }

    @Test
    void addIngredientByRecipeId() {
    }

    @Test
    void deleteIngredientByRecipeId() {
    }

    @Test
    void getRecipes() {
    }

    @Test
    void createRecipe() {
    }

    @Test
    void updateRecipe() {
    }

    @Test
    void deleteRecipe() {
    }
}
